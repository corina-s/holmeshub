# write an app that plays songs from spotify about detectives and prints the artist name
import spotipy

def play_detective_songs(session):
    sp = spotipy.Spotify(auth=session['token'])
    results = sp.search(q='detective', type='track')
    for track in results['tracks']['items']:
        print(track['artists'][0]['name'])
